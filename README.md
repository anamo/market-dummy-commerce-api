# Market Dummy Commerce API

A dummy implementation of the Cloud Commerce API.

shell

```shell

$ git clone git@bitbucket.org:anamo/market-dummy-commerce-api.git
$ composer install

```

apache-conf

```apache-conf

Define static_path "/path/to/folder"
Define http_port "port_number"
Include "${static_path}/config/apache2.conf"

```
