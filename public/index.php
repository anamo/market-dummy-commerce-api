<?php /*! anamo/market-dummy-commerce-api v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/market-dummy-commerce-api */

header('Content-type: application/json; charset=utf-8');

$document = [];

register_shutdown_function(function() use(&$document) {
	$document['jsonapi'] = ['version' => 'dummy'];
	$document['links'] = ['self' => $_SERVER['REQUEST_URI']];
	//$document['meta'] = ['stamp' => $_SERVER['REQUEST_TIME'], 'node' => $_SERVER['HTTP_HOST'], 'lang' => 'en_IE', 'cost' => '0 seconds'];
	echo json_encode($document);
});

switch($_SERVER['REQUEST_METHOD']) {
	case 'DELETE':
		// TODO:
		break;

	case 'PATCH':
		// TODO:
		break;

	case 'GET':
		list(, $version, $resource, $id) = explode('/', parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH), 4);
		if(!empty($id) && !(@include 'resources/'.$resource.'.one.php')) {
			http_response_code(404);
		} elseif(!(@include 'resources/'.$resource.'.all.php')) {
      http_response_code(404);
		}
		break;

	case 'POST':
		// TODO:
		break;

}
