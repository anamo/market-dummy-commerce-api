<?php /*! anamo/market-dummy-commerce-api v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/market-dummy-commerce-api */

$document = [
	'meta' => ['total'=>1,'ipp'=>999,'last_page'=>1,'page'=>1,'next_page'=>1,'previous_page'=>0],
	'data' =>
	[
		[
			'type' => 'return-policies',
      'id' => '000000000000000000000000000001',
      'attributes' =>
      [
        'effective-date' => '2019-01-28T11:38:23.422Z',
				'locale' => 'en_IE',
				'html_content' => '<p>Samsung</p>',
			],
			'relationships' => [
				'bullets' =>
				[
					'data' =>
					[
						[
							'type' => 'return-policy-bullets',
							'id' => '000000000000000000000000000001',
						],
						[
							'type' => 'return-policy-bullets',
							'id' => '000000000000000000000000000002',
						],
					],
				],
			]
		]
	],
	'included' =>
	[
		[
      'type' => 'return-policy-bullets',
      'id' => '000000000000000000000000000001',
			'attributes' =>
			[
				'type' => 'icon',
				'locale' => 'en_IE',
				'text' => 'Customer can receive a refund',
			],
      'relationships' =>
      [
        'policy' =>
        [
          'data' =>
          [
            'type' => 'return-policies',
            'id' => '000000000000000000000000000001',
					],
				]
      ],
		],
		[
      'type' => 'return-policy-bullets',
      'id' => '000000000000000000000000000002',
			'attributes' =>
			[
				'type' => 'icon',
				'locale' => 'en_IE',
				'text' => 'No exchanges',
			],
      'relationships' =>
      [
        'policy' =>
        [
          'data' =>
          [
            'type' => 'return-policies',
            'id' => '000000000000000000000000000001',
					],
				]
      ],
		]
	]
];

//die(json_encode($document,JSON_UNESCAPED_UNICODE));
