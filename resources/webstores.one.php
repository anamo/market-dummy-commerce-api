<?php /*! anamo/market-dummy-commerce-api v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/market-dummy-commerce-api */

$str = <<<'EOD'
{
  "data": {
      "type": "webstores",
      "id": "anamo-grr",
      "attributes": {
          "name": "anamo-grr",
          "date-created": "2017-07-26 08:57:50.000"
      },
      "relationships": {
          "product-design": {
              "data": {
                  "type": "webstores-product-designs",
                  "id": "6554232344576587877"
              }
          }
      },
      "links": {
          "self": "/webstores/anamo-grr"
      },
      "meta": {
          "some": "metadata for anamo-grr"
      }
  },
  "included": [
      {
          "type": "webstores-product-designs",
          "id": "6554232344576587877",
          "attributes": {
              "illustration-ratio": "default",
              "info-design": "default",
              "heart-wishlist": "illustration-top-right",
              "manufacturer": "info",
              "sku": "info",
              "new": "illustration-top-left",
              "sale": "illustration-top-left",
              "exclusive": "illustration-top-left",
              "stock": "default"
          },
          "links": {
              "self": "/webstores-product-designs/6554232344576587877"
          },
          "meta": {
              "some": "metadata for 6554232344576587877"
          }
      }
  ]
}
EOD;

$document = json_decode($str, true);
