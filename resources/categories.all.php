<?php /*! anamo/market-dummy-commerce-api v1.0.0 | © 2006-present Anamo Inc. MIT License | bitbucket.org/anamo/market-dummy-commerce-api */


if('tilefona/kinita-smartphones/1111' == $_GET['slug']) {
	$str = <<<'EOD'
{
	"meta": {
		"current-page": 1,
		"last-page": 1,
		"page-cursor": 0,
		"page-size": 999,
		"total": 1,
		"stamp": 1560243115,
    "cost": "0 seconds"
	},
	"data": [{
		"type": "categories",
		"id": "1111",
		"attributes": {
			"left": 1,
			"right": 10,
			"depth": 0
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "1111a"
				}]
			}
		}
	}],
	"included": [{
		"type": "category-i18ns",
		"id": "1111a",
		"attributes": {
			"locale": "el_GR",
			"text": "CLOTHING",
			"seo": "Μπες Anamo.gr και βρες προσφορές σε προϊόντα τεχνολογίας & αθλητισμού στις χαμηλότερες τιμές! Smartphones, tablets, gaming, fitness, σε ασυναγώνιστες τιμές και με άμεση παράδοση.",
			"user-slug": "sale",
			"slug": "sale\/1111"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "1111"
				}
			}
		}
	}]
}
EOD;

} elseif('tilefona/smartphones/2222' == $_GET['slug']) {
	$str = <<<'EOD'
{
	"meta": {
		"current-page": 1,
		"last-page": 1,
		"page-cursor": 0,
		"page-size": 999,
		"total": 1,
		"stamp": 1560243115,
    "cost": "0 seconds"
	},
	"data": [{
		"type": "categories",
		"id": "2222",
		"attributes": {
			"left": 2,
			"right": 9,
			"depth": 1
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "2222a"
				}]
			}
		}
	}],
	"included": [{
		"type": "category-i18ns",
		"id": "2222a",
		"attributes": {
			"locale": "el_GR",
			"text": "WOMEN",
			"seo": "Μπες Anamo.gr και βρες προσφορές σε προϊόντα τεχνολογίας & αθλητισμού στις χαμηλότερες τιμές! Smartphones, tablets, gaming, fitness, σε ασυναγώνιστες τιμές και με άμεση παράδοση.",
			"user-slug": "sale",
			"slug": "sale\/2222"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "2222"
				}
			}
		}
	}]
}
EOD;

} elseif('tilefona/katiallo/3333' == $_GET['slug']) {
	$str = <<<'EOD'
{
	"meta": {
		"current-page": 1,
		"last-page": 1,
		"page-cursor": 0,
		"page-size": 999,
		"total": 1,
		"stamp": 1560243115,
    "cost": "0 seconds"
	},
	"data": [{
		"type": "categories",
		"id": "3333",
		"attributes": {
			"left": 3,
			"right": 8,
			"depth": 2
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "3333a"
				}]
			}
		}
	}],
	"included": [{
		"type": "category-i18ns",
		"id": "3333a",
		"attributes": {
			"locale": "el_GR",
			"text": "DRESSES",
			"seo": "Μπες Anamo.gr και βρες προσφορές σε προϊόντα τεχνολογίας & αθλητισμού στις χαμηλότερες τιμές! Smartphones, tablets, gaming, fitness, σε ασυναγώνιστες τιμές και με άμεση παράδοση.",
			"user-slug": "sale",
			"slug": "sale\/2222"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "3333"
				}
			}
		}
	}]
}
EOD;

} elseif('tilefona/pame/4444' == $_GET['slug']) {
	$str = <<<'EOD'
{
	"meta": {
		"current-page": 1,
		"last-page": 1,
		"page-cursor": 0,
		"page-size": 999,
		"total": 1,
		"stamp": 1560243115,
    "cost": "0 seconds"
	},
	"data": [{
		"type": "categories",
		"id": "4444",
		"attributes": {
			"left": 4,
			"right": 5,
			"depth": 3
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "4444a"
				}]
			}
		}
	}],
	"included": [{
		"type": "category-i18ns",
		"id": "4444a",
		"attributes": {
			"locale": "el_GR",
			"text": "LONG DRESSES",
			"seo": "Μπες Anamo.gr και βρες προσφορές σε προϊόντα τεχνολογίας & αθλητισμού στις χαμηλότερες τιμές! Smartphones, tablets, gaming, fitness, σε ασυναγώνιστες τιμές και με άμεση παράδοση.",
			"user-slug": "sale",
			"slug": "sale\/4444"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "4444"
				}
			}
		}
	}]
}
EOD;

} elseif('tilefona/tora/5555' == $_GET['slug']) {
	$str = <<<'EOD'
{
	"meta": {
		"current-page": 1,
		"last-page": 1,
		"page-cursor": 0,
		"page-size": 999,
		"total": 1,
		"stamp": 1560243115,
    "cost": "0 seconds"
	},
	"data": [{
		"type": "categories",
		"id": "5555",
		"attributes": {
			"left": 6,
			"right": 7,
			"depth": 3
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "5555a"
				}]
			}
		}
	}],
	"included": [{
		"type": "category-i18ns",
		"id": "5555a",
		"attributes": {
			"locale": "el_GR",
			"text": "MIDI DRESSES",
			"seo": "Μπες Anamo.gr και βρες προσφορές σε προϊόντα τεχνολογίας & αθλητισμού στις χαμηλότερες τιμές! Smartphones, tablets, gaming, fitness, σε ασυναγώνιστες τιμές και με άμεση παράδοση.",
			"user-slug": "sale",
			"slug": "sale\/5555"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "5555"
				}
			}
		}
	}]
}
EOD;

} else {

	$str = <<<'EOD'
{
	"meta": {
		"current-page": 1,
		"last-page": 1,
		"page-cursor": 0,
		"page-size": 999,
		"total": 84,
		"stamp": 1560243115,
    "cost": "0 seconds"
	},
	"data": [{
		"type": "categories",
		"id": "1111",
		"attributes": {
			"left": 1,
			"right": 10,
			"depth": 0
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "1111a"
				}]
			}
		}
	},
	{
		"type": "categories",
		"id": "2222",
		"attributes": {
			"left": 2,
			"right": 9,
			"depth": 1
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "2222a"
				}]
			}
		}
	},
	{
		"type": "categories",
		"id": "3333",
		"attributes": {
			"left": 3,
			"right": 8,
			"depth": 2
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "3333a"
				}]
			}
		}
	},
	{
		"type": "categories",
		"id": "4444",
		"attributes": {
			"left": 4,
			"right": 5,
			"depth": 3
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "4444a"
				}]
			}
		}
	},
	{
		"type": "categories",
		"id": "5555",
		"attributes": {
			"left": 6,
			"right": 7,
			"depth": 3
		},
		"relationships": {
			"webstore": {
				"data": {
					"type": "webstores",
					"id": "anamo-gr"
				}
			},
			"i18ns": {
				"data": [{
					"type": "category-i18ns",
					"id": "5555a"
				}]
			}
		}
	}],
	"included": [{
		"type": "category-i18ns",
		"id": "1111a",
		"attributes": {
			"locale": "el_GR",
			"text": "CLOTHING",
			"seo": "",
			"user-slug": "tilefona\/kinita-smartphones",
			"slug": "tilefona\/kinita-smartphones\/1111"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "1111"
				}
			}
		}
	},
	{
		"type": "category-i18ns",
		"id": "2222a",
		"attributes": {
			"locale": "el_GR",
			"text": "WOMEN",
			"seo": "",
			"user-slug": "tilefona\/smartphones",
			"slug": "tilefona\/smartphones\/2222"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "2222"
				}
			}
		}
	},
	{
		"type": "category-i18ns",
		"id": "3333a",
		"attributes": {
			"locale": "el_GR",
			"text": "DRESSES",
			"seo": "",
			"user-slug": "tilefona\/katiallo",
			"slug": "tilefona\/katiallo\/3333"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "3333"
				}
			}
		}
	},
	{
		"type": "category-i18ns",
		"id": "4444a",
		"attributes": {
			"locale": "el_GR",
			"text": "LONG DRESSES",
			"seo": "",
			"user-slug": "tilefona\/katiallo",
			"slug": "tilefona\/pame\/4444"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "4444"
				}
			}
		}
	},
	{
		"type": "category-i18ns",
		"id": "5555a",
		"attributes": {
			"locale": "el_GR",
			"text": "MIDI DRESSES",
			"seo": "",
			"user-slug": "tilefona\/tora",
			"slug": "tilefona\/tora\/5555"
		},
		"relationships": {
			"category": {
				"data": {
					"type": "categories",
					"id": "5555"
				}
			}
		}
	}]
}
EOD;
}

$document = json_decode($str, true);
